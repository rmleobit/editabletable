import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class CopyPasteService {
    private readonly rowSeparator = '\n';
    private readonly colSeparator = '\t';

    public parse(text: string): string[][] {
        const rows = text.split(this.rowSeparator);
        const values = _.map(rows, row => row.split(this.colSeparator));

        return values;
    }

    public stringify(values: string[][]): string {
        let stringifiedValues = '';
        const lastIndex = values.length - 1;
        for (let i = 0; i < lastIndex; ++i) {
            stringifiedValues += values[i].join(this.colSeparator) + this.rowSeparator;
        }

        stringifiedValues += values[lastIndex].join(this.colSeparator);
        return stringifiedValues;
    }
}
