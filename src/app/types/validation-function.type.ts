export type ValidationFunction = (value: string) => boolean;
