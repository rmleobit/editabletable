import * as _ from 'lodash';
import { ValidationFunction } from '../types/validation-function.type';

export class DatumModel {
    value: string;
    validators: ValidationFunction[];

    get isValid(): boolean {
        return _.every(this.validators, v => v(this.value));
    }

    private _isSelected: boolean = false;
    get isSelected(): boolean {
        return this._isSelected
    }

    private _isDisabled: boolean = true;
    get isDisabled(): boolean {
        return this._isDisabled
    }

    private _isHighlighted: boolean = false;
    get isHighlighted(): boolean {
        return this._isHighlighted
    }

    public constructor(init?: Partial<DatumModel>) {
        _.assign(this, init);
    }

    public deselectAndDisable(): void {
        this._isSelected = false;
        this._isDisabled = true;
    }

    public select(): void {
        this._isSelected = true;
    }

    public enable(): void {
        this._isDisabled = false;
    }

    public highlight(): void {
        this._isHighlighted = true;
    }

    public unhighlight(): void {
        this._isHighlighted = false;
    }
}