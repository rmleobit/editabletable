import * as _ from 'lodash';

export class DatumCoordsModel {
    rowIndex: number;
    colIndex: number;

    public constructor(init?: Partial<DatumCoordsModel>) {
        _.assign(this, init);
    }
}
