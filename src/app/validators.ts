export class Validators {
    public static Number(value: any): boolean {
        return !isNaN(parseFloat(value)) && isFinite(value);
    }
}