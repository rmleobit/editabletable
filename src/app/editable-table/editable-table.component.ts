import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from 'lodash';
import { DatumModel } from '../models/datum.model';
import { Validators } from '../validators';
import { KeyCode } from '../constants/key-code.enum';
import { CopyPasteService } from '../services/copy-paste.service';
import { DatumCoordsModel } from '../models/datum-coords.model';

@Component({
    selector: 'app-editable-table',
    templateUrl: './editable-table.component.html',
    styleUrls: ['./editable-table.component.scss']
})
export class EditableTableComponent implements OnInit, OnDestroy {
    public data: DatumModel[][];

    public get rowsNumber() {
        return this.data.length;
    }

    public get colsNumber() {
        return _.first(this.data).length;
    }

    private selectedDatumCoords: DatumCoordsModel;
    public get selectedDatum(): DatumModel {
        return this.data[this.selectedDatumCoords.rowIndex][this.selectedDatumCoords.colIndex];
    }

    private isMouseDown: boolean = false;
    private firstHighlightedDatumCoords: DatumCoordsModel;
    private lastHighlightedDatumCoords: DatumCoordsModel;

    private readonly keyboardEventType = 'keydown';
    private onDocumentKeyboardEnterBinded: any;
    private onDocumentKeyboardArrowsBinded: any;

    public constructor(private copyPasteService: CopyPasteService) { }

    public ngOnInit(): void {
        this.initData();
        this.initDatumCoords();
        this.addKeyboardListeners();
    }

    public ngOnDestroy(): void {
        this.removeKeyboardListeners();
    }

    private initData(): void {
        this.data = [];
        const rowNumber = 5;
        const colNumber = 5;
        _.each(_.range(rowNumber), rowIndex => {
            this.data[rowIndex] = [];
            _.each(_.range(colNumber), colIndex => {
                this.data[rowIndex][colIndex] = new DatumModel({value: '', validators: []});
            })
        });
    }

    private initDatumCoords(): void {
        const initRowIndex = 0;
        const initColIndex = 0;

        this.selectedDatumCoords = { rowIndex: initRowIndex, colIndex: initColIndex };
        this.selectedDatum.select();

        this.firstHighlightedDatumCoords = { rowIndex: initRowIndex, colIndex: initColIndex };
        this.lastHighlightedDatumCoords = { rowIndex: initRowIndex, colIndex: initColIndex };
    }

    private addKeyboardListeners(): void {
        this.onDocumentKeyboardEnterBinded = this.onDocumentKeyboardEnter.bind(this);
        this.onDocumentKeyboardArrowsBinded = this.onDocumentKeyboardArrows.bind(this);
        document.addEventListener(this.keyboardEventType, this.onDocumentKeyboardEnterBinded);
        document.addEventListener(this.keyboardEventType, this.onDocumentKeyboardArrowsBinded);
    }

    private removeKeyboardListeners(): void {
        document.removeEventListener(this.keyboardEventType, this.onDocumentKeyboardEnterBinded);
        document.removeEventListener(this.keyboardEventType, this.onDocumentKeyboardArrowsBinded);
    }

    public range(value: number): number[] {
        return _.range(value);
    }

    public onDatumFocus(rowIndex: number, colIndex: number): void {
        this.selectDatum(rowIndex, colIndex);
    }

    public onDatumDblClick(rowIndex: number, colIndex: number): void {
        this.selectDatum(rowIndex, colIndex, true);
    }

    public onDatumMouseDown(rowIndex: number, colIndex: number): void {
        let startRowIndex = this.firstHighlightedDatumCoords.rowIndex;
        let endRowIndex = this.lastHighlightedDatumCoords.rowIndex;
        if (startRowIndex > endRowIndex) {
            startRowIndex = this.lastHighlightedDatumCoords.rowIndex;
            endRowIndex = this.firstHighlightedDatumCoords.rowIndex;
        }

        let startColIndex = this.firstHighlightedDatumCoords.colIndex;
        let endColIndex = this.lastHighlightedDatumCoords.colIndex;
        if (startColIndex > endColIndex) {
            startColIndex = this.lastHighlightedDatumCoords.colIndex;
            endColIndex = this.firstHighlightedDatumCoords.colIndex;
        }

        this.unhighlight(startRowIndex, endRowIndex, startColIndex, endColIndex);

        this.isMouseDown = true;
        this.firstHighlightedDatumCoords = { rowIndex, colIndex };
        this.lastHighlightedDatumCoords = { rowIndex, colIndex }
    }

    public onDatumMouseUp(): void {
        this.isMouseDown = false;
    }

    public onMouseLeave(): void {
        this.isMouseDown = false;
    }

    public onDatumHover(rowIndex: number, colIndex: number): void {
        if (!this.isMouseDown) {
            return;
        }

        if (this.lastHighlightedDatumCoords.rowIndex != rowIndex) {
            this.onRowHover(rowIndex, colIndex);
        }

        if (this.lastHighlightedDatumCoords.colIndex != colIndex) {
            this.onColHover(rowIndex, colIndex);
        }

        if (this.firstHighlightedDatumCoords.rowIndex == rowIndex && this.firstHighlightedDatumCoords.colIndex == colIndex) {
            this.data[this.firstHighlightedDatumCoords.rowIndex][this.firstHighlightedDatumCoords.colIndex].unhighlight();
        }
        else {
            this.data[this.firstHighlightedDatumCoords.rowIndex][this.firstHighlightedDatumCoords.colIndex].highlight();
        }

        this.lastHighlightedDatumCoords = { rowIndex, colIndex };
    }

    private onRowHover(rowIndex: number, colIndex: number): void {
        let startColIndex = this.firstHighlightedDatumCoords.colIndex;
        let endColIndex = colIndex;
        if (startColIndex > endColIndex) {
            startColIndex = colIndex;
            endColIndex = this.firstHighlightedDatumCoords.colIndex;
        }

        if (this.isRowHoverFromBottom(rowIndex)) {
            if (this.lastHighlightedDatumCoords.rowIndex < rowIndex) {
                this.highlightRow(rowIndex, startColIndex, endColIndex);
            }
            else {
                this.unhighlightRow(this.lastHighlightedDatumCoords.rowIndex, startColIndex, endColIndex);
            }
        }
        else {
            if (this.lastHighlightedDatumCoords.rowIndex < rowIndex) {
                this.unhighlightRow(this.lastHighlightedDatumCoords.rowIndex, startColIndex, endColIndex);
            }
            else {
                this.highlightRow(rowIndex, startColIndex, endColIndex);
            }
        }
    }

    private isRowHoverFromBottom(rowIndex: number): boolean {
        return this.firstHighlightedDatumCoords.rowIndex < rowIndex
            || this.firstHighlightedDatumCoords.rowIndex < this.lastHighlightedDatumCoords.rowIndex
    }

    private highlightRow(rowIndex: number, startColIndex: number, endColIndex: number): void {
        for (let j = startColIndex; j <= endColIndex; ++j) {
            this.data[rowIndex][j].highlight();
        }
    }

    private unhighlightRow(rowIndex: number, startColIndex: number, endColIndex: number): void {
        for (let j = startColIndex; j <= endColIndex; ++j) {
            this.data[rowIndex][j].unhighlight();
        }
    }

    private onColHover(rowIndex: number, colIndex: number): void {
        let startRowIndex = this.firstHighlightedDatumCoords.rowIndex;
        let endRowIndex = rowIndex;
        if (startRowIndex > endRowIndex) {
            startRowIndex = rowIndex;
            endRowIndex = this.firstHighlightedDatumCoords.rowIndex;
        }

        if (this.isColHoverFromRight(colIndex)) {
            if (this.lastHighlightedDatumCoords.colIndex < colIndex) {
                this.highlightCol(colIndex, startRowIndex, endRowIndex);
            }
            else {
                this.unhighlightCol(this.lastHighlightedDatumCoords.colIndex, startRowIndex, endRowIndex);
            }
        }
        else {
            if (this.lastHighlightedDatumCoords.colIndex < colIndex) {
                this.unhighlightCol(this.lastHighlightedDatumCoords.colIndex, startRowIndex, endRowIndex);
            }
            else {
                this.highlightCol(colIndex, startRowIndex, endRowIndex);
            }
        }
    }

    private isColHoverFromRight(colIndex: number): boolean {
        return this.firstHighlightedDatumCoords.colIndex < colIndex
            || this.firstHighlightedDatumCoords.colIndex < this.lastHighlightedDatumCoords.colIndex
    }

    private highlightCol(colIndex: number, startRowIndex: number, endRowIndex: number): void {
        for (let i = startRowIndex; i <= endRowIndex; ++i) {
            this.data[i][colIndex].highlight();
        }
    }

    private unhighlightCol(colIndex: number, startRowIndex: number, endRowIndex: number): void {
        for (let i = startRowIndex; i <= endRowIndex; ++i) {
            this.data[i][colIndex].unhighlight();
        }
    }

    private unhighlight(startRowIndex: number, endRowIndex: number, startColIndex: number, endColIndex: number): void {
        for (let i = startRowIndex; i <= endRowIndex; ++i) {
            for (let j = startColIndex; j <= endColIndex; ++j) {
                this.data[i][j].unhighlight();
            }
        }
    }

    public onPaste(event: ClipboardEvent): boolean {
        const pastedText = event.clipboardData.getData('text/plain');
        const values = this.copyPasteService.parse(pastedText);
        for (let i = 0; i < values.length; ++i) {
            const rowIndex = this.selectedDatumCoords.rowIndex + i;
            if (rowIndex >= this.rowsNumber) {
                break;
            }

            for(let j = 0; j < values[i].length; ++j) {
                const colIndex = this.selectedDatumCoords.colIndex + j;
                if (rowIndex >= this.colsNumber) {
                    break;
                }

                const datum = this.data[rowIndex][colIndex];
                    datum.value = values[i][j];
            }
        }

        return false;
    }

    public onCopy(event: ClipboardEvent): void {
        let startRowIndex = this.firstHighlightedDatumCoords.rowIndex;
        let endRowIndex = this.lastHighlightedDatumCoords.rowIndex;
        if (startRowIndex > endRowIndex) {
            startRowIndex = this.lastHighlightedDatumCoords.rowIndex;
            endRowIndex = this.firstHighlightedDatumCoords.rowIndex;
        }

        let startColIndex = this.firstHighlightedDatumCoords.colIndex;
        let endColIndex = this.lastHighlightedDatumCoords.colIndex;
        if (startColIndex > endColIndex) {
            startColIndex = this.lastHighlightedDatumCoords.colIndex;
            endColIndex = this.firstHighlightedDatumCoords.colIndex;
        }

        const values: string[][] = [];
        let rowNumber = 0;
        for (let i = startRowIndex; i <= endRowIndex; ++i) {
            values[rowNumber] = [];
            for (let j = startColIndex; j <= endColIndex; ++j) {
                values[rowNumber].push(this.data[i][j].value);
            }

            ++rowNumber;
        }

        const stringifiedValues = this.copyPasteService.stringify(values);
        event.clipboardData.setData('text/plain', stringifiedValues);
    }

    public onDocumentKeyboardEnter(event: KeyboardEvent): void {
        if (event.keyCode !== KeyCode.Enter) {
            return;
        }

        let rowIndex = this.selectedDatumCoords.rowIndex;
        const colIndex = this.selectedDatumCoords.colIndex;
        if (this.selectedDatum.isDisabled) {
            this.selectedDatum.enable();
        }
        else {
            if (rowIndex < this.rowsNumber - 1) {
                ++rowIndex;
            }

            this.selectDatum(rowIndex, colIndex);
        }
    }

    public onDocumentKeyboardArrows(event: KeyboardEvent): void {
        let rowIndex = this.selectedDatumCoords.rowIndex;
        let colIndex = this.selectedDatumCoords.colIndex;

        switch (event.keyCode) {
            case KeyCode.ArrowDown: {
                if (rowIndex < this.rowsNumber - 1) {
                    ++rowIndex;
                }

                break;
            }
            case KeyCode.ArrowUp: {
                if (rowIndex - 1 >= 0) {
                    --rowIndex;
                }

                break;
            }
            case KeyCode.ArrowLeft: {
                if (colIndex - 1 >= 0) {
                    --colIndex;
                }

                break;
            }
            case KeyCode.ArrowRight: {
                if (colIndex < this.colsNumber - 1) {
                    ++colIndex;
                }

                break;
            }
            default:
                return;
        }

        this.selectDatum(rowIndex, colIndex);
    }

    private selectDatum(rowIndex: number, colIndex: number, isEnabling: boolean = false): void {
        this.selectedDatum.deselectAndDisable();

        const datum = this.data[rowIndex][colIndex];
        datum.select();
        if (isEnabling) {
            datum.enable();
        }

        this.selectedDatumCoords.rowIndex = rowIndex;
        this.selectedDatumCoords.colIndex = colIndex;
    }
}
