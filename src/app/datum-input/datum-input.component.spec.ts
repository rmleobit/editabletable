import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatumInputComponent } from './datum-input.component';

describe('DatumInputComponent', () => {
  let component: DatumInputComponent;
  let fixture: ComponentFixture<DatumInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatumInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatumInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
