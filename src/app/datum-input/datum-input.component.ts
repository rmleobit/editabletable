import { Component, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { KeyCode } from '../constants/key-code.enum';

@Component({
    selector: 'app-datum-input',
    templateUrl: './datum-input.component.html',
    styleUrls: ['./datum-input.component.scss']
})
export class DatumInputComponent {
    @ViewChild('input') input: ElementRef;

    @Input() value: string;
    @Output() valueChange = new EventEmitter<string>();

    private _isDisabled: boolean;
    @Input() public set isDisabled(value: boolean) {
        if (!value) {
            this.focusInput();
        }

        this._isDisabled = value;
    }

    public get isDisabled(): boolean {
        return this._isDisabled;
    }

    public onInput(value: string): void {
        this.valueChange.emit(value);
    }

    public onKeyDown(event: KeyboardEvent): void {
        if (event.keyCode !== KeyCode.Enter) {
            event.stopPropagation();
        }
    }

    public onPaste(event: ClipboardEvent): boolean {
        if (!this.isDisabled) {
            event.stopPropagation();
            return true;
        }

        return false;
    }

    public onCopy(event: ClipboardEvent): boolean {
        if (!this.isDisabled) {
            event.stopPropagation();
            return true;
        }

        return false;
    }

    public onMouseDown(event: MouseEvent): void {
        if (!this.isDisabled) {
            event.stopPropagation();
        }
    }

    private focusInput(): void {
        const element = this.input.nativeElement as HTMLInputElement;
        element.disabled = false;
        element.focus();
    }
}
